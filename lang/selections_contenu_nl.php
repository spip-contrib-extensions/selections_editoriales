<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/selections_contenu?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// A
	'ajouter_lien_selections_contenu' => 'Voeg de geselecteerde inhoud toe',

	// C
	'champ_css_explication' => 'Een of meerdere toe te voegen CSS classes.',
	'champ_css_label' => 'CSS classes',
	'champ_descriptif_label' => 'Omschrijving',
	'champ_id_selection_label' => 'Selectie',
	'champ_titre_explication' => 'Wanneer de titel leeg is, wordt deze op magische wijze gevuld.',
	'champ_titre_label' => 'Titel',
	'champ_url_explication' => 'De URL van een pagina (http://voorbeeld) of de koppeling naar inhoud van SPIP (article123, rubrique456, enz).',
	'champ_url_label' => 'URL',

	// D
	'deplacer' => 'De inhoud verschuiven',
	'deplacer_apres' => 'Verplaatsen na',
	'deplacer_avant' => 'Verplaatsen voor',

	// I
	'icone_creer_selections_contenu' => 'Maak de geselecteerde inhoud',
	'icone_modifier_selections_contenu' => 'Pas de geselecteerde inhoud aan',
	'info_1_selections_contenu' => 'Eén geselecteerde inhoud',
	'info_aucun_selections_contenu' => 'Geen geselecteerde inhoud',
	'info_nb_selections_contenus' => '@nb@  geselecteerde items',
	'info_selections_contenus_auteur' => 'De geselecteerde inhoud van deze auteur',

	// R
	'retirer_lien_selections_contenu' => 'verwijder de geselecteerde inhoud',
	'retirer_tous_liens_selections_contenus' => 'Verwijder alle geselecteerde inhoud',

	// T
	'texte_ajouter_selections_contenu' => 'Een geselecteerde inhoud toevoegen',
	'texte_changer_statut_selections_contenu' => 'Deze geselecteerde inhoud is:',
	'texte_creer_associer_selections_contenu' => 'Maak en koppel een geselecteerde inhoud',
	'titre_langue_selections_contenu' => 'Taal van deze geselecteerde inhoud',
	'titre_logo_selections_contenu' => 'Logo van deze geselecteerde inhoud',
	'titre_selections_contenu' => 'Geselecteerde inhoud',
	'titre_selections_contenus' => 'Geselecteerde inhoud',
	'titre_selections_contenus_rubrique' => 'Geselecteerde inhoud van de rubriek',
];
