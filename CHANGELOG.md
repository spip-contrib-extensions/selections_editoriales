# Changelog

## [Unreleased]

## 3.1.1 - 2024-12-20

### Fixed

 - Ajout de `rector.php` et normalisation du code [UP_TO_SPIP_41]
 - Syntaxe simplifiée des fichiers de langue 

## 3.1.0 - 2024-07-05

### Fixed

- Ajouter un contenu à une sélection liée à objet n'ouvre plus le formulaire sur toutes les sélections
- Recherche réparée dans le formulaire d'ajout de sélection
- Warning PHP 8.2 en moins lorsqu'on rattache une sélection à une rubrique