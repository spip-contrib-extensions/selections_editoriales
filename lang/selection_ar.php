<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/selection?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// A
	'ajouter_lien_selection' => 'إضافة هذا المحتوى المفضّل',

	// B
	'bouton_enlever_selection' => 'إزالة الربط',
	'bouton_modifier_selection' => 'تعديل',
	'bouton_supprimer_selection' => 'حذف',

	// C
	'champ_css_explication' => 'أنماط css مضافة الى هذا الاختيار.',
	'champ_css_label' => 'Css',
	'champ_descriptif_label' => 'الوصف',
	'champ_identifiant_explication' => 'معرّف نصي فريد يتيح جلب هذا الاختيار بسهولة.',
	'champ_identifiant_label' => 'المعرّف',
	'champ_limite_explication' => 'الحد الاقصى للمحتويات في الاختيار.',
	'champ_limite_label' => 'الحد الاقصى',
	'champ_nombre_contenus' => 'عدد العناصر',
	'champ_titre_label' => 'العنوان',

	// E
	'erreur_identifiant_existant' => 'هذا المعرّف مستخدم سابقاً في الاختيار «@selection@».',
	'erreur_limite_entier' => 'يجب ان يكون الحد الاقصى عدد صحيح موجب.',

	// I
	'icone_creer_selection' => 'إنشاء مفضّل تحريري',
	'icone_modifier_selection' => 'تعديل هذا المفضّل التحريري',
	'info_1_selection' => 'مفضّل تحريري واحد',
	'info_aucun_selection' => 'لا يوجد اي مفضّل تحريري',
	'info_nb_selections' => '@nb@ مفضّل تحريري',
	'info_selections_auteur' => 'المفضّلات التحريرية لهذا المؤلف',

	// R
	'retirer_lien_selection' => 'سحب هذا المفضّل التحريري',
	'retirer_tous_liens_selections' => 'سحب كل المفضّلات التحريرية',

	// T
	'texte_ajouter_selection' => 'إضافة مفضّل تحريري',
	'texte_ajouter_selection_explication' => 'إنشاء مفضّل تحريري او اختيار واحد من القائمة أدناه.',
	'texte_changer_statut_selection' => 'هذا المفضّل التحريري:',
	'texte_creer_associer_selection' => 'إنشاء مفضّل تحريري وربطه',
	'titre_langue_selection' => 'لغة هذا المفضّل التحريري',
	'titre_logo_selection' => 'رمز هذا المفضّل التحريري',
	'titre_nouvelle_selection' => 'مفضّل تحريري جديد',
	'titre_selection' => 'المفضّل التحريري',
	'titre_selection_nouvelle' => 'الاختيار',
	'titre_selections' => 'المفضّلات التحريرية',
	'titre_selections_autonomes' => 'مفضّلات تحريرية مستقلة',
	'titre_selections_objets' => 'مفضّلات تحريرية مرتبطة بمحتوى',
	'titre_selections_rubrique' => 'مفضّلات القسم التحريرية',
];
