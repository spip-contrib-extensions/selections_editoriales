<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-selections_editoriales?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// S
	'selections_editoriales_description' => 'انشاء لوائح محتوى بإعطائها عناوين وترتيب ووصف وربط صور بها. يمكن استخدام هذه الوظيفة لإدارة التحرير في الصفحة الأساسية للموقع مثلاً.',
	'selections_editoriales_nom' => 'اختيار المفضل التحريري',
	'selections_editoriales_slogan' => 'انشاد لوائح من العناصر المهمة في الموقع',
];
