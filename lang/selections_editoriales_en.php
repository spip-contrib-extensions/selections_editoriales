<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/selections_editoriales?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// C
	'configurer_objets_label' => 'Allow appending editorial selections on content:',

	// I
	'info_aucune_utilisation' => 'This selection isn’t linked to any content.',
	'info_des_utilisations' => '@nb@ uses',
	'info_une_utilisation' => '1 use',

	// S
	'selection_vue' => 'Selections seen in the text',
	'selections_editoriales_titre' => 'Editorial selection',

	// T
	'titre_page_configurer_selections_editoriales' => 'Editorial selections settings',
];
