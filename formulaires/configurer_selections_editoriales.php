<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclaration des saisies
 */
function formulaires_configurer_selections_editoriales_saisies_dist() {

	include_spip('inc/config');

	$saisies = [
		[
			'saisie' => 'choisir_objets',
			'options' => [
				'nom' => 'objets',
				'label' => _T('selections_editoriales:configurer_objets_label'),
				'exclus' => ['spip_selections', 'spip_selections_contenus'],
				'defaut' => lire_config('selections_editoriales/objets'),
			],
		],
		[
			'saisie' => 'case',
			'options' => [
				'nom' => 'reutilisation',
				'label_case' => _T('selections_editoriales:configurer_reutilisation_label_case'),
				'explication' => _T('selections_editoriales:configurer_reutilisation_explication'),
				'defaut' => lire_config('selections_editoriales/reutilisation'),
			],
		],
		[
			'saisie' => 'case',
			'options' => [
				'nom' => 'accueil_prive_orphelines',
				'label_case' => _T('selections_editoriales:configurer_accueil_prive_orphelines_label_case'),
				'defaut' => lire_config('selections_editoriales/accueil_prive_orphelines'),
			],
		],
	];

	return $saisies;
}
