<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/selection?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// A
	'ajouter_lien_selection' => 'Voeg deze redactionele selectie toe',

	// B
	'bouton_enlever_selection' => 'Ontkoppelen',
	'bouton_modifier_selection' => 'Aanpassen',
	'bouton_supprimer_selection' => 'Verwijderen',

	// C
	'champ_css_explication' => 'Aan deze selectie toegevoegde CSS class.',
	'champ_css_label' => 'CSS',
	'champ_descriptif_label' => 'Omschrijving',
	'champ_identifiant_explication' => 'Een unieke tekstidentificatie die de selectie gemakkelijk selecteerbaar maakt.',
	'champ_identifiant_label' => 'Identificatie',
	'champ_limite_explication' => 'Maximum aantal items in de selectie.',
	'champ_limite_label' => 'Limiet',
	'champ_nombre_contenus' => 'Aantal elementen',
	'champ_titre_label' => 'Titel',

	// E
	'erreur_identifiant_existant' => 'Deze identificatie wordt al gebruikt voor selectie "@selection@".',
	'erreur_limite_entier' => 'De limiet moet een geheel positief getal zijn.',

	// I
	'icone_creer_selection' => 'Een redactionele selectie maken',
	'icone_modifier_selection' => 'Deze redactionele selectie aanpassen',
	'info_1_selection' => 'Een redactionele selectie',
	'info_aucun_selection' => 'Geen redactionele selectie',
	'info_nb_selections' => '@nb@ redactionele selecties',
	'info_selections_auteur' => 'De redactionele selecties van deze auteur',

	// R
	'retirer_lien_selection' => 'Deze redactionele selectie intrekken',
	'retirer_tous_liens_selections' => 'Alle redactionele selecties intrekken',

	// T
	'texte_ajouter_selection' => 'Een redactionele selectie toevoegen',
	'texte_ajouter_selection_explication' => 'Maak een nieuwe editoriale selectie of kies er een uit onderstaande lijst.',
	'texte_changer_statut_selection' => 'Deze redactionele selectie is:',
	'texte_creer_associer_selection' => 'Een redactionele selectie maken en koppelen',
	'titre_langue_selection' => 'Taal van deze redactionele selectie',
	'titre_logo_selection' => 'Logo van deze redactionele selectie',
	'titre_nouvelle_selection' => 'Nieuwe redactionele selectie',
	'titre_selection' => 'Redactionele selectie',
	'titre_selection_nouvelle' => 'Selectie',
	'titre_selections' => 'Redactionele selecties',
	'titre_selections_autonomes' => 'Zelfstandige redactionele selecties',
	'titre_selections_objets' => 'Aan inhoud gekoppelde redactionele selecties',
	'titre_selections_rubrique' => 'Redactionele selecties van de rubriek',
];
