<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/selections_contenu?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// A
	'ajouter_lien_selections_contenu' => 'إضافة هذا المحتوى المختار',

	// C
	'champ_css_explication' => 'إضافة نمط CSS او اكثر الى هذا المحتوى المختار.',
	'champ_css_label' => 'أنماط CSS',
	'champ_descriptif_label' => 'الوصف',
	'champ_id_selection_label' => 'المواد المختارة',
	'champ_titre_explication' => 'اذا بقي العنوان فارغاً، قد يتم ملؤه بشكل عجيب.',
	'champ_titre_label' => 'العنوان',
	'champ_url_explication' => 'عنوان صفحة (http://exemple) او اختصار محتوى من SPIP (مثلاً article123، rubrique456،...).',
	'champ_url_label' => 'عنوان URL',

	// D
	'deplacer' => 'سحب ونقل هذا المحتوى',
	'deplacer_apres' => 'نقل الى ما بعد',
	'deplacer_avant' => 'نقل الى ما قبل',

	// I
	'icone_creer_selections_contenu' => 'آنشاء محتوى مفضّل',
	'icone_modifier_selections_contenu' => 'تغيير هذا المحتوى المفضّل',
	'info_1_selections_contenu' => 'محتوى مفضّل',
	'info_aucun_selections_contenu' => 'لا يوجد اي محتوى مفضّل',
	'info_nb_selections_contenus' => '@nb@  محتويات مفضّلة',
	'info_selections_contenus_auteur' => 'المحتويات المفضلة لهذا المؤلف',

	// R
	'retirer_lien_selections_contenu' => 'سحب هذا المحتوى المفضّل',
	'retirer_tous_liens_selections_contenus' => 'سحب كل المحتويات المفضّلة',

	// T
	'texte_ajouter_selections_contenu' => 'إضافة محتوى مفضّل',
	'texte_changer_statut_selections_contenu' => 'هذا المحتوى المفضّل هو:',
	'texte_creer_associer_selections_contenu' => 'إنشاء نحتوى مفضّل وربطه',
	'titre_langue_selections_contenu' => 'لغة هذا المحتوى المفضّل',
	'titre_logo_selections_contenu' => 'رمز هذا المحتوى المفضّل',
	'titre_selections_contenu' => 'محتوى مفضّل',
	'titre_selections_contenus' => 'محتويات مفضّلة',
	'titre_selections_contenus_rubrique' => 'محتويات القسم المفضّلة',
];
