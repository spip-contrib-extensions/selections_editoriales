<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use SpipLeague\Component\Rector\Set\SpipSetList;
use SpipLeague\Component\Rector\Set\SpipLevelSetList;

return RectorConfig::configure()
    ->withPaths([
        __DIR__ . '/action',
        __DIR__ . '/base',
        __DIR__ . '/formulaires',
        __DIR__ . '/inc',
        __DIR__ . '/lang',        
    ])
    ->withSets(
        [
            SpipSetList::SPIP_41,
            SpipLevelSetList::UP_TO_SPIP_41
        ],
    )
;